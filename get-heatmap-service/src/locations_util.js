let MongoClient = require("mongodb").MongoClient;
let responses = require("./responses");
let config = require("./config").get();
let format = require("./format");
let mongodb = require("mongodb");

let image_util = require("./image_util")

module.exports.getLocations = event => {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(
      `mongodb://${config["username"]}:${config["password"]}@${config["url"]}/${config["db_name"]}`,
      { useUnifiedTopology: true },
      function(err, client) {
        if (err) {
          console.log(err);
          resolve(responses.responseFailure(err));
        } else {
          let db = client.db(config["db_name"]);
          selectLocations(
            db,
            function() {
              client.close();
            },
            event.body.bounding_box
          ).then(arr => resolve(responses.responseLocations(arr)));
        }
      }
    );
  });
};

module.exports.insertLocation = event => {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(
      `mongodb://${config["username"]}:${config["password"]}@${config["url"]}/${config["db_name"]}`,
      { useUnifiedTopology: true },
      function(err, client) {
        if (err) {
          console.log(err);
          resolve(responses.responseFailure(err));
        } else {
          try {
            let db = client.db(config["db_name"]);
            addLocation(
              db,
              format.jsonMultipart(event.body),
              getBase64(event),
              event.file.mimetype,
              function() {
                client.close();
              }
            )
            resolve(responses.responseSuccess)
          } catch (err) {
            console.log(err);
            resolve(responses.responseFailure(err));
          }
        }
      }
    );
  });
};

module.exports.deleteLocation = event => {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(
      `mongodb://${config["username"]}:${config["password"]}@${config["url"]}/${config["db_name"]}`,
      { useUnifiedTopology: true },
      function(err, client) {
        if (err) {
          console.log(err);
          resolve(responses.responseFailure(err));
        } else {
          try {
            let db = client.db(config["db_name"]);
            deleteLocation(db, event.body.id, function() {
              client.close();
            }).then(data => resolve(responses.responseSuccess));
          } catch (err) {
            console.log(err);
            resolve(responses.responseFailure(err));
          }
        }
      }
    );
  });
};

module.exports.getClusters = (bbox) => {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(
      `mongodb://${config["username"]}:${config["password"]}@${config["url"]}/${config["db_name"]}`,
      { useUnifiedTopology: true },
      function(err, client) {
        if (err) {
          console.log(err);
          resolve(responses.responseFailure(err));
        } else {
          let db = client.db(config["db_name"]);
          selectLocations(
            db,
            function() {
              client.close();
            },
            bbox
          ).then(arr =>
            resolve(
              responses.responseClusters(
                toClusters(arr)
              )
            )
          );
        }
      }
    );
  });
};

function addLocation(db, loc, file, type, callback) {
  return new Promise(function(resolve, reject) {
    image_util.classifyImage(file).then((passed) => {
      let new_record = {
        location: {
          type: "Point",
          coordinates: [loc.coordinates.lat, loc.coordinates.lon]
        },
        repeats_count: 1,
        radius: calclRadius(loc.category),
        received_requests: [
          {
            timestamp: Date.now(),
            user_coefficient: calculateUserCoefficient()
          }
        ],
        category: loc.category,
        verified: false,
        ai_passed: passed,
        fileBase64: file,
        fileType: type
      };
      db.createCollection("locations", function(err, results) {
        db.collection("locations").insertOne(new_record, function(err, r) {
          callback();
          resolve(true);
        });
      });
    })
    
  });
}

function calclRadius(category) {
  switch(category) {
    case "queue":
      return 30;
    case "public gathering":
      return 100;;
    case "heavy pedestrian traffic":
      return 70;
  default:
      return 10;
  }
}

function deleteLocation(db, id, callback) {
  return new Promise(function(resolve, reject) {
    db.collection("locations").deleteOne(
      { _id: new mongodb.ObjectID(id) },
      function(err, r) {
        callback();
        resolve(true);
      }
    );
  });
}

function selectLocations(db, callback, bbox) {
  return new Promise(function(resolve, reject) {
    db.collection("locations")
      .find({
        $and : [
          {
            location: {
              $geoWithin: {
                $box: [
                  format.coordinatesToArr(bbox.coordinates_bottom_left),
                  format.coordinatesToArr(bbox.coordinates_top_right)
                ]
              }
            }
          },
          {
            verified : { $eq : true }
          }
        ]
      })
      .toArray((err, locArr) => {
        if (err) console.log(err);
        callback();
        resolve(format.locationsArray(locArr));
      });
  });
}

function toClusters(points, bias) {

  let coordinates = format.pointsToCoordinates(points);

  return coordinates;
}

function calculateUserCoefficient() {
  return 1;
}

function getBase64(event) {
  return event.file.buffer.toString("base64");
}
