"use strict";

let express = require("express");
let multer = require("multer");
let storage = multer.memoryStorage();
let upload = multer({ storage: storage });
let bodyParser = require("body-parser");
let https = require("https")
let fs = require("fs")

let location_util = require("./locations_util");
let maps_util = require("./maps_util");
let dashboard_util = require("./dashboard_util");
let cors = require('cors')


const app = express();
app.use(cors())

app.post("/new_location", upload.single("file"), async (req, res) => {
  const responsePromise = location_util.insertLocation(req);

  responsePromise.then(response => res.json(response));
});

app.post("/get_locations", bodyParser.json({ type: '*/*' }), async (req, res) => {
  const responsePromise = location_util.getLocations(req);

  responsePromise.then(response => res.json(response));
});

app.post("/get_safe_route", bodyParser.json({ type: '*/*' }), async (req, res) => {
  const responsePromise = maps_util.getRoute(req);

  responsePromise.then(response => res.json(response));
});

app.post("/delete_location", bodyParser.json({ type: '*/*' }),  async (req, res) => {
  const responsePromise = location_util.deleteLocation(req);

  responsePromise.then(response => res.json(response));
});

app.get("/get_unverified_locations", bodyParser.json({ type: '*/*' }),  async (req, res) => {
  const responsePromise = dashboard_util.getUnverifiedLocations(req);

  responsePromise.then(response => res.json(response));
});

app.post("/approve_location", bodyParser.json({ type: '*/*' }), async (req, res) => {
  const responsePromise = dashboard_util.approveLocation(req);

  responsePromise.then(response => res.json(response));
});

app.post("/change_category", bodyParser.json({ type: '*/*' }),  async (req, res) => {
  const responsePromise = dashboard_util.changeCategory(req);

  responsePromise.then(response => res.json(response));
});

app.post("/get_photos", bodyParser.json({ type: '*/*' }),  async (req, res) => {
  const responsePromise = dashboard_util.getPhotos(req);

  responsePromise.then(response => res.json(response));
});

app.get("/get_photo",  async (req, res) => {
  const responsePromise = dashboard_util.getPhoto(req);

  responsePromise.then(response => res.set('Content-Type', response.photo.type).send(Buffer.from(response.photo.base64, 'base64')));
});

app.listen("3001", () => console.log(`crowdradar-service listening`));

// https.createServer({
//   key: fs.readFileSync('server.key'),
//   cert: fs.readFileSync('server.cert')
// }, app)
// .listen(3001, function () {
//   console.log('crowdradar-service listening! Go to https://localhost:3001/')
// })

// module.exports.clusters = async (event, context) => {
//   context.callbackWaitsForEmptyEventLoop = false;

//   const responsePromise = location_util.getClusters(event);

//   return responsePromise;
// };

// module.exports.route = async (event, context) => {
//   context.callbackWaitsForEmptyEventLoop = false;

//   const responsePromise = google_util.getRoute(event);

//   return responsePromise;
// };

// module.exports.place = async (event, context) => {
//   context.callbackWaitsForEmptyEventLoop = false;

//   const responsePromise = google_util.getPlace(event);

//   return responsePromise;
// };

// module.exports.addmarker = async (event, context) => {
//   context.callbackWaitsForEmptyEventLoop = false;

//   const responsePromise = marker_util.addMarker(event);

//   return responsePromise;
// };

// module.exports.getmarkers = async (event, context) => {
//   context.callbackWaitsForEmptyEventLoop = false;

//   const responsePromise = marker_util.getMarkers(event);

//   return responsePromise;
// };
