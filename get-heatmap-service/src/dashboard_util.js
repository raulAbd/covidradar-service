let MongoClient = require("mongodb").MongoClient;
let responses = require("./responses");
let config = require("./config").get();
let format = require("./format");
let mongodb = require("mongodb");

module.exports.getUnverifiedLocations = event => {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(
      `mongodb://${config["username"]}:${config["password"]}@${config["url"]}/${config["db_name"]}`,
      { useUnifiedTopology: true },
      function(err, client) {
        if (err) {
          console.log(err);
          resolve(responses.responseFailure(err));
        } else {
          let db = client.db(config["db_name"]);
          selectUnverifiedLocations(db, function() {
            client.close();
          }).then(arr => resolve(responses.responseLocations(arr)));
        }
      }
    );
  });
};

module.exports.approveLocation = event => {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(
      `mongodb://${config["username"]}:${config["password"]}@${config["url"]}/${config["db_name"]}`,
      { useUnifiedTopology: true },
      function(err, client) {
        if (err) {
          console.log(err);
          resolve(responses.responseFailure(err));
        } else {
          let db = client.db(config["db_name"]);
          approveLocation(
            db,
            function() {
              client.close();
            },
            event.body.id
          ).then(arr => resolve(responses.responseSuccess));
        }
      }
    );
  });
};

module.exports.changeCategory = event => {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(
      `mongodb://${config["username"]}:${config["password"]}@${config["url"]}/${config["db_name"]}`,
      { useUnifiedTopology: true },
      function(err, client) {
        if (err) {
          console.log(err);
          resolve(responses.responseFailure(err));
        } else {
          let db = client.db(config["db_name"]);
          changeCategory(
            db,
            function() {
              client.close();
            },
            event.body.id,
            event.body.category
          ).then(arr => resolve(responses.responseSuccess));
        }
      }
    );
  });
};

module.exports.getPhotos = event => {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(
      `mongodb://${config["username"]}:${config["password"]}@${config["url"]}/${config["db_name"]}`,
      { useUnifiedTopology: true },
      function(err, client) {
        if (err) {
          console.log(err);
          resolve(responses.responseFailure(err));
        } else {
          let db = client.db(config["db_name"]);
          selectLocationsByIds(
            db,
            format.toObjectIds(event.body.ids),
            function() {
              client.close();
            }
          ).then(arr => resolve(responses.responsePhotos(arr)));
        }
      }
    );
  });
};

module.exports.getPhoto = event => {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(
      `mongodb://${config["username"]}:${config["password"]}@${config["url"]}/${config["db_name"]}`,
      { useUnifiedTopology: true },
      function(err, client) {
        if (err) {
          console.log(err);
          resolve(responses.responseFailure(err));
        } else {
          let db = client.db(config["db_name"]);
          selectLocation(db, event.query.id, function() {
            client.close();
          }).then(arr => resolve(responses.responsePhoto(arr)));
        }
      }
    );
  });
};

function selectLocationsByIds(db, ids, callback) {
  return new Promise(function(resolve, reject) {
    db.collection("locations")
      .find({ _id: { $in: ids } })
      .toArray((err, locArr) => {
        if (err) console.log(err);
        callback();
        resolve(format.photos(locArr));
      });
  });
}

function selectLocation(db, id, callback) {
  return new Promise(function(resolve, reject) {
    db.collection("locations")
      .findOne({ _id : new mongodb.ObjectID(id) },
        (err, doc) => {
        if (err) console.log(err);
        callback();
        if(doc == null) resolve({ photo : "Id not found" })
        resolve(format.photo(doc));
      });
  });
}

function selectUnverifiedLocations(db, callback) {
  return new Promise(function(resolve, reject) {
    db.collection("locations")
      .find({
        verified: { $eq: false }
      })
      .toArray((err, locArr) => {
        if (err) console.log(err);
        callback();
        resolve(format.unverifiedLocations(locArr));
      });
  });
}

function approveLocation(db, callback, id) {
  return new Promise(function(resolve, reject) {
    db.collection("locations").findOneAndUpdate(
      { _id: new mongodb.ObjectID(id) },
      { $set: { verified: true } },
      (err, doc) => {
        if (err) console.log(err);
        callback();
        resolve(doc);
      }
    );
  });
}

function changeCategory(db, callback, id, category) {
  return new Promise(function(resolve, reject) {
    db.collection("locations").findOneAndUpdate(
      { _id: new mongodb.ObjectID(id) },
      { $set: { category: category } },
      (err, doc) => {
        if (err) console.log(err);
        callback();
        resolve(doc);
      }
    );
  });
}
